.PHONY: install uninstall

PREFIX ?= /usr
BINDIR?=$(PREFIX)/bin
DOCDIR?=$(PREFIX)/share/doc/astro
MANDIR?=$(PREFIX)/share/man/man1
LICENSEDIR?=$(PREFIX)/share/doc/astro

install:
	mkdir -p $(DESTDIR)$(BINDIR) $(DESTDIR)$(DOCDIR) $(DESTDIR)$(LICENSEDIR) $(DESTDIR)$(MANDIR)
	install -m +rx astro -t $(DESTDIR)$(BINDIR)
	install -m +r README.md CONTRIBUTING.md -t $(DESTDIR)$(DOCDIR)
	install -m +r LICENSE -t $(DESTDIR)$(LICENSEDIR)
	install -m +r astro.en.1 $(DESTDIR)$(MANDIR)/astro.1

uninstall:
	rm $(DESTDIR)$(BINDIR)/astro
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -rf $(DESTDIR)$(LICENSEDIR)
	rm -f $(DESTDIR)$(MANDIR)/astro.1
